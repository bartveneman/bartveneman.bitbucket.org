/*global Core*/
(function (Core, $, Echo) {
    "use strict";

    // Run all modules from a single for-loop
    var module;

    for (module in Core) {
        // Check if modules exist and if they have a setup method
        if (Core.hasOwnProperty(module) && Core[module].hasOwnProperty("setup")) {
            Core[module].setup();
        }
    }

    /*****************************************************************
     * GENERAL METHODS USED THROUGHOUT SITE                          *
     ****************************************************************/
    Core.StackEqualizer.equalize($(".Order"), function () {
        return this.find(".Order__Value");
    });

    // @note Disabled until issue with image loading is fixed.
    // Core.StackEqualizer.equalize($(".Matrix"), function () {
    //    return this.children("li");
    // });

    // @note Core.ContentAppender is called from DOM directly

    // Apply lazy loading for images
    Echo.init();

    /*****************************************************************
     * ACCOUNT                                                       *
     ****************************************************************/
    $(".OrderRulesToggler").on("click", function () {
        $(this).toggleClass("OrderRulesToggler--IsOpen").prev().slideToggle(Core.Settings.ANIMATION_DURATION);
    });

}(Core, jQuery, Echo));
