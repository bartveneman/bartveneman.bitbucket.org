/**
 * @namespace CoreSettings
 * @memberof  Core
 */
var Core = (function () {
    "use strict";

    var /**
         * The default animation duration
         *
         * @memberof! Core.CoreSettings
         * @name ANIMATION_DURATION
         * @type {number}
         * @public
         */
        ANIMATION_DURATION = 150,

        /**
         * Breakpoint reference. Should reflect those in CSS.
         *
         * @memberof! Core.CoreSettings
         * @name BREAKPOINTS
         * @type {object}
         * @public
         */
        BREAKPOINTS = {
            "x-small":    "28em",
            "small":      "33em",
            "medium":     "55em",
            "large":      "66em",
            "x-large":    "80em",
            "xx-large":  "110em",
            "xxx-large": "130em"
        },

        /**
         * Return a Media Query string
         *
         * @function mediaQuery
         * @memberof! Core.CoreSettings
         * @public
         * @param {string} breakpoint A breakpoint from the list above
         * @param {bool} min Whether to use min-width or max-width. Default: min-width.
         * @return {string} The complete media query string.
         */
        mediaQuery = function (breakpoint, min) {
            var mobileFirst = min === false ? "max" : "min";

            return "screen and (" + mobileFirst + "-width: " + breakpoint + ")";
        };

    return Core.register("Settings", {
        ANIMATION_DURATION: ANIMATION_DURATION,
        BREAKPOINTS: BREAKPOINTS,
        mediaQuery: mediaQuery
    });
}(Core || {}));
