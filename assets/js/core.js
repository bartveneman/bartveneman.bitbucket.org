/**
 * @namespace Core
 */
var Core = (function () {
    "use strict";

    var /**
         * Register a new module to the Core object
         *
         * @function register
         * @memberOf! Core
         * @param  {string} name   Name of the module.
         * @param  {Object} module The actual module.
         * @return {Object}        The updated Core.
         * @public
         */
        register = function (name, module) {
            if (!this[name]) {
                this[name] = module;
            }

            return this;
        },

        /**
         * Empty function to use for callbacks etc. instead of having 
         * to make numerous empty functions yourself. Should save some 
         * memory.
         * 
         * @function noop
         * @memberOf! Core
         * @public
         */
        noop = function () {};

    return {
        register: register,
        noop: noop
    };
}());
