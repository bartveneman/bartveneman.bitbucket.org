/*! Echo v1.5.0 | (c) 2014 @toddmotto | MIT license | github.com/toddmotto/echo */
window.Echo = (function (global, document, undefined) {

  'use strict';

  /**
   * callback - initialized to a no-op so that no validations on it's presence need to be made
   * @type {Function}
   */
  var callback = function(){};

  /**
   * offset, throttle, poll, unload vars
   */
  var offset, throttle, poll, unload;

  /**
   *  _inView
   * @private
   * @param {Element} element Image element
   * @returns {Boolean} Is element in viewport
   */
  var _inView = function (element, view) {
    var box = element.getBoundingClientRect();
    return (box.right >= view.l && box.bottom >= view.t && box.left <= view.r && box.top <= view.b);
  };

  /**
   * _pollImages Loop through the images if present
   * or remove all event listeners
   * @private
   */
  var _pollImages = function () {
    var src,
        i,
        elem,
        view,
        nodes = document.querySelectorAll('img[data-echo]'),
        length = nodes.length;
    view = {
      l: 0 - offset.l,
      t: 0 - offset.t,
      b: (window.innerHeight || document.documentElement.clientHeight) + offset.b,
      r: (window.innerWidth || document.documentElement.clientWidth) + offset.r
    };
    for(i=0; i<length; i++) {
      elem = nodes[i];
      if(_inView(elem, view)) {
        if(unload) {
          elem.setAttribute('data-echo-placeholder', elem.src);
        }
        elem.src = elem.getAttribute('data-echo');
        if(!unload) {
          elem.removeAttribute('data-echo');
        }
        callback(elem, 'load');
      } else if(unload && !!(src = elem.getAttribute('data-echo-placeholder'))) {
        elem.src = src;
        elem.removeAttribute('data-echo-placeholder');
        callback(elem, 'unload');
      }
    }
    if(!length) {
      detach();
    }
  };

  /**
   * _throttle Sensible event firing
   * @private
   */
  var _throttle = function () {
    clearTimeout(poll);
    poll = setTimeout(_pollImages, throttle);
  };

  /**
   * init Module init function
   * @param {Object} [opts] Passed in Object with options
   * @param {Number|String} [opts.throttle]
   * @param {Number|String} [opts.offset]
   * @param {Number|String} [opts.offsetBottom]
   * @param {Number|String} [opts.offsetTop]
   * @param {Number|String} [opts.offsetLeft]
   * @param {Number|String} [opts.offsetRight]
   * @param {Boolean} [opts.unload]
   * @param {Function} [opts.callback]
   */
  var init = function (opts) {

    opts = opts || {};
    var offsetAll = opts.offset || 0;
    var offsetVertical = opts.offsetVertical || offsetAll;
    var offsetHorizontal = opts.offsetHorizontal || offsetAll;

    function optionToInt(opt, fallback) {
      return parseInt(opt || fallback, 10);
    }

    offset = {
      t: optionToInt(opts.offsetTop, offsetVertical),
      b: optionToInt(opts.offsetBottom, offsetVertical),
      l: optionToInt(opts.offsetLeft, offsetHorizontal),
      r: optionToInt(opts.offsetRight, offsetHorizontal)
    };
    throttle = optionToInt(opts.throttle, 250);
    unload = !!opts.unload;
    callback = opts.callback || callback;


    _pollImages();

    if (document.addEventListener) {
      global.addEventListener('scroll', _throttle, false);
      global.addEventListener('load', _throttle, false);
    } else {
      global.attachEvent('onscroll', _throttle);
      global.attachEvent('onload', _throttle);
    }

  };

  /**
   * detach remove listeners
   */
  var detach = function() {
    if (document.removeEventListener) {
      global.removeEventListener('scroll', _throttle);
    } else {
      global.detachEvent('onscroll', _throttle);
    }
    clearTimeout(poll);
  };

  /**
   * return Public methods
   * @returns {Object}
   */
  return {
    init: init,
    detach: detach,
    render: _pollImages
  };

})(this, document);

/*!
 * enquire.js v2.1.0 - Awesome Media Queries in JavaScript
 * Copyright (c) 2014 Nick Williams - http://wicky.nillia.ms/enquire.js
 * License: MIT (http://www.opensource.org/licenses/mit-license.php)
 */

;(function (name, context, factory) {
    var matchMedia = window.matchMedia;

    if (typeof module !== 'undefined' && module.exports) {
        module.exports = factory(matchMedia);
    }
    else if (typeof define === 'function' && define.amd) {
        define(function() {
            return (context[name] = factory(matchMedia));
        });
    }
    else {
        context[name] = factory(matchMedia);
    }
}('enquire', this, function (matchMedia) {

    'use strict';

    /*jshint unused:false */
    /**
     * Helper function for iterating over a collection
     *
     * @param collection
     * @param fn
     */
    function each(collection, fn) {
        var i      = 0,
            length = collection.length,
            cont;

        for(i; i < length; i++) {
            cont = fn(collection[i], i);
            if(cont === false) {
                break; //allow early exit
            }
        }
    }

    /**
     * Helper function for determining whether target object is an array
     *
     * @param target the object under test
     * @return {Boolean} true if array, false otherwise
     */
    function isArray(target) {
        return Object.prototype.toString.apply(target) === '[object Array]';
    }

    /**
     * Helper function for determining whether target object is a function
     *
     * @param target the object under test
     * @return {Boolean} true if function, false otherwise
     */
    function isFunction(target) {
        return typeof target === 'function';
    }

    /**
     * Delegate to handle a media query being matched and unmatched.
     *
     * @param {object} options
     * @param {function} options.match callback for when the media query is matched
     * @param {function} [options.unmatch] callback for when the media query is unmatched
     * @param {function} [options.setup] one-time callback triggered the first time a query is matched
     * @param {boolean} [options.deferSetup=false] should the setup callback be run immediately, rather than first time query is matched?
     * @constructor
     */
    function QueryHandler(options) {
        this.options = options;
        !options.deferSetup && this.setup();
    }
    QueryHandler.prototype = {

        /**
         * coordinates setup of the handler
         *
         * @function
         */
        setup : function() {
            if(this.options.setup) {
                this.options.setup();
            }
            this.initialised = true;
        },

        /**
         * coordinates setup and triggering of the handler
         *
         * @function
         */
        on : function() {
            !this.initialised && this.setup();
            this.options.match && this.options.match();
        },

        /**
         * coordinates the unmatch event for the handler
         *
         * @function
         */
        off : function() {
            this.options.unmatch && this.options.unmatch();
        },

        /**
         * called when a handler is to be destroyed.
         * delegates to the destroy or unmatch callbacks, depending on availability.
         *
         * @function
         */
        destroy : function() {
            this.options.destroy ? this.options.destroy() : this.off();
        },

        /**
         * determines equality by reference.
         * if object is supplied compare options, if function, compare match callback
         *
         * @function
         * @param {object || function} [target] the target for comparison
         */
        equals : function(target) {
            return this.options === target || this.options.match === target;
        }

    };
    /**
     * Represents a single media query, manages it's state and registered handlers for this query
     *
     * @constructor
     * @param {string} query the media query string
     * @param {boolean} [isUnconditional=false] whether the media query should run regardless of whether the conditions are met. Primarily for helping older browsers deal with mobile-first design
     */
    function MediaQuery(query, isUnconditional) {
        this.query = query;
        this.isUnconditional = isUnconditional;
        this.handlers = [];
        this.mql = matchMedia(query);

        var self = this;
        this.listener = function(mql) {
            self.mql = mql;
            self.assess();
        };
        this.mql.addListener(this.listener);
    }
    MediaQuery.prototype = {

        /**
         * add a handler for this query, triggering if already active
         *
         * @param {object} handler
         * @param {function} handler.match callback for when query is activated
         * @param {function} [handler.unmatch] callback for when query is deactivated
         * @param {function} [handler.setup] callback for immediate execution when a query handler is registered
         * @param {boolean} [handler.deferSetup=false] should the setup callback be deferred until the first time the handler is matched?
         */
        addHandler : function(handler) {
            var qh = new QueryHandler(handler);
            this.handlers.push(qh);

            this.matches() && qh.on();
        },

        /**
         * removes the given handler from the collection, and calls it's destroy methods
         * 
         * @param {object || function} handler the handler to remove
         */
        removeHandler : function(handler) {
            var handlers = this.handlers;
            each(handlers, function(h, i) {
                if(h.equals(handler)) {
                    h.destroy();
                    return !handlers.splice(i,1); //remove from array and exit each early
                }
            });
        },

        /**
         * Determine whether the media query should be considered a match
         * 
         * @return {Boolean} true if media query can be considered a match, false otherwise
         */
        matches : function() {
            return this.mql.matches || this.isUnconditional;
        },

        /**
         * Clears all handlers and unbinds events
         */
        clear : function() {
            each(this.handlers, function(handler) {
                handler.destroy();
            });
            this.mql.removeListener(this.listener);
            this.handlers.length = 0; //clear array
        },

        /*
         * Assesses the query, turning on all handlers if it matches, turning them off if it doesn't match
         */
        assess : function() {
            var action = this.matches() ? 'on' : 'off';

            each(this.handlers, function(handler) {
                handler[action]();
            });
        }
    };
    /**
     * Allows for registration of query handlers.
     * Manages the query handler's state and is responsible for wiring up browser events
     *
     * @constructor
     */
    function MediaQueryDispatch () {
        if(!matchMedia) {
            throw new Error('matchMedia not present, legacy browsers require a polyfill');
        }

        this.queries = {};
        this.browserIsIncapable = !matchMedia('only all').matches;
    }

    MediaQueryDispatch.prototype = {

        /**
         * Registers a handler for the given media query
         *
         * @param {string} q the media query
         * @param {object || Array || Function} options either a single query handler object, a function, or an array of query handlers
         * @param {function} options.match fired when query matched
         * @param {function} [options.unmatch] fired when a query is no longer matched
         * @param {function} [options.setup] fired when handler first triggered
         * @param {boolean} [options.deferSetup=false] whether setup should be run immediately or deferred until query is first matched
         * @param {boolean} [shouldDegrade=false] whether this particular media query should always run on incapable browsers
         */
        register : function(q, options, shouldDegrade) {
            var queries         = this.queries,
                isUnconditional = shouldDegrade && this.browserIsIncapable;

            if(!queries[q]) {
                queries[q] = new MediaQuery(q, isUnconditional);
            }

            //normalise to object in an array
            if(isFunction(options)) {
                options = { match : options };
            }
            if(!isArray(options)) {
                options = [options];
            }
            each(options, function(handler) {
                queries[q].addHandler(handler);
            });

            return this;
        },

        /**
         * unregisters a query and all it's handlers, or a specific handler for a query
         *
         * @param {string} q the media query to target
         * @param {object || function} [handler] specific handler to unregister
         */
        unregister : function(q, handler) {
            var query = this.queries[q];

            if(query) {
                if(handler) {
                    query.removeHandler(handler);
                }
                else {
                    query.clear();
                    delete this.queries[q];
                }
            }

            return this;
        }
    };

    return new MediaQueryDispatch();

}));

/* Modernizr 2.8.1 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-touch-shiv-cssclasses-teststyles-prefixes-css_vhunit
 */
;



window.Modernizr = (function( window, document, undefined ) {

    var version = '2.8.0',

    Modernizr = {},

    enableClasses = true,

    docElement = document.documentElement,

    mod = 'modernizr',
    modElem = document.createElement(mod),
    mStyle = modElem.style,

    inputElem  ,


    toString = {}.toString,

    prefixes = ' -webkit- -moz- -o- -ms- '.split(' '),



    tests = {},
    inputs = {},
    attrs = {},

    classes = [],

    slice = classes.slice,

    featureName, 


    injectElementWithStyles = function( rule, callback, nodes, testnames ) {

      var style, ret, node, docOverflow,
          div = document.createElement('div'),
                body = document.body,
                fakeBody = body || document.createElement('body');

      if ( parseInt(nodes, 10) ) {
                      while ( nodes-- ) {
              node = document.createElement('div');
              node.id = testnames ? testnames[nodes] : mod + (nodes + 1);
              div.appendChild(node);
          }
      }

                style = ['&#173;','<style id="s', mod, '">', rule, '</style>'].join('');
      div.id = mod;
          (body ? div : fakeBody).innerHTML += style;
      fakeBody.appendChild(div);
      if ( !body ) {
                fakeBody.style.background = '';
                fakeBody.style.overflow = 'hidden';
          docOverflow = docElement.style.overflow;
          docElement.style.overflow = 'hidden';
          docElement.appendChild(fakeBody);
      }

      ret = callback(div, rule);
        if ( !body ) {
          fakeBody.parentNode.removeChild(fakeBody);
          docElement.style.overflow = docOverflow;
      } else {
          div.parentNode.removeChild(div);
      }

      return !!ret;

    },
    _hasOwnProperty = ({}).hasOwnProperty, hasOwnProp;

    if ( !is(_hasOwnProperty, 'undefined') && !is(_hasOwnProperty.call, 'undefined') ) {
      hasOwnProp = function (object, property) {
        return _hasOwnProperty.call(object, property);
      };
    }
    else {
      hasOwnProp = function (object, property) { 
        return ((property in object) && is(object.constructor.prototype[property], 'undefined'));
      };
    }


    if (!Function.prototype.bind) {
      Function.prototype.bind = function bind(that) {

        var target = this;

        if (typeof target != "function") {
            throw new TypeError();
        }

        var args = slice.call(arguments, 1),
            bound = function () {

            if (this instanceof bound) {

              var F = function(){};
              F.prototype = target.prototype;
              var self = new F();

              var result = target.apply(
                  self,
                  args.concat(slice.call(arguments))
              );
              if (Object(result) === result) {
                  return result;
              }
              return self;

            } else {

              return target.apply(
                  that,
                  args.concat(slice.call(arguments))
              );

            }

        };

        return bound;
      };
    }

    function setCss( str ) {
        mStyle.cssText = str;
    }

    function setCssAll( str1, str2 ) {
        return setCss(prefixes.join(str1 + ';') + ( str2 || '' ));
    }

    function is( obj, type ) {
        return typeof obj === type;
    }

    function contains( str, substr ) {
        return !!~('' + str).indexOf(substr);
    }


    function testDOMProps( props, obj, elem ) {
        for ( var i in props ) {
            var item = obj[props[i]];
            if ( item !== undefined) {

                            if (elem === false) return props[i];

                            if (is(item, 'function')){
                                return item.bind(elem || obj);
                }

                            return item;
            }
        }
        return false;
    }
    tests['touch'] = function() {
        var bool;

        if(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
          bool = true;
        } else {
          injectElementWithStyles(['@media (',prefixes.join('touch-enabled),('),mod,')','{#modernizr{top:9px;position:absolute}}'].join(''), function( node ) {
            bool = node.offsetTop === 9;
          });
        }

        return bool;
    };
    for ( var feature in tests ) {
        if ( hasOwnProp(tests, feature) ) {
                                    featureName  = feature.toLowerCase();
            Modernizr[featureName] = tests[feature]();

            classes.push((Modernizr[featureName] ? '' : 'no-') + featureName);
        }
    }



     Modernizr.addTest = function ( feature, test ) {
       if ( typeof feature == 'object' ) {
         for ( var key in feature ) {
           if ( hasOwnProp( feature, key ) ) {
             Modernizr.addTest( key, feature[ key ] );
           }
         }
       } else {

         feature = feature.toLowerCase();

         if ( Modernizr[feature] !== undefined ) {
                                              return Modernizr;
         }

         test = typeof test == 'function' ? test() : test;

         if (typeof enableClasses !== "undefined" && enableClasses) {
           docElement.className += ' ' + (test ? '' : 'no-') + feature;
         }
         Modernizr[feature] = test;

       }

       return Modernizr; 
     };


    setCss('');
    modElem = inputElem = null;

    ;(function(window, document) {
                var version = '3.7.0';

            var options = window.html5 || {};

            var reSkip = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i;

            var saveClones = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i;

            var supportsHtml5Styles;

            var expando = '_html5shiv';

            var expanID = 0;

            var expandoData = {};

            var supportsUnknownElements;

        (function() {
          try {
            var a = document.createElement('a');
            a.innerHTML = '<xyz></xyz>';
                    supportsHtml5Styles = ('hidden' in a);

            supportsUnknownElements = a.childNodes.length == 1 || (function() {
                        (document.createElement)('a');
              var frag = document.createDocumentFragment();
              return (
                typeof frag.cloneNode == 'undefined' ||
                typeof frag.createDocumentFragment == 'undefined' ||
                typeof frag.createElement == 'undefined'
              );
            }());
          } catch(e) {
                    supportsHtml5Styles = true;
            supportsUnknownElements = true;
          }

        }());

            function addStyleSheet(ownerDocument, cssText) {
          var p = ownerDocument.createElement('p'),
          parent = ownerDocument.getElementsByTagName('head')[0] || ownerDocument.documentElement;

          p.innerHTML = 'x<style>' + cssText + '</style>';
          return parent.insertBefore(p.lastChild, parent.firstChild);
        }

            function getElements() {
          var elements = html5.elements;
          return typeof elements == 'string' ? elements.split(' ') : elements;
        }

            function getExpandoData(ownerDocument) {
          var data = expandoData[ownerDocument[expando]];
          if (!data) {
            data = {};
            expanID++;
            ownerDocument[expando] = expanID;
            expandoData[expanID] = data;
          }
          return data;
        }

            function createElement(nodeName, ownerDocument, data){
          if (!ownerDocument) {
            ownerDocument = document;
          }
          if(supportsUnknownElements){
            return ownerDocument.createElement(nodeName);
          }
          if (!data) {
            data = getExpandoData(ownerDocument);
          }
          var node;

          if (data.cache[nodeName]) {
            node = data.cache[nodeName].cloneNode();
          } else if (saveClones.test(nodeName)) {
            node = (data.cache[nodeName] = data.createElem(nodeName)).cloneNode();
          } else {
            node = data.createElem(nodeName);
          }

                                                    return node.canHaveChildren && !reSkip.test(nodeName) && !node.tagUrn ? data.frag.appendChild(node) : node;
        }

            function createDocumentFragment(ownerDocument, data){
          if (!ownerDocument) {
            ownerDocument = document;
          }
          if(supportsUnknownElements){
            return ownerDocument.createDocumentFragment();
          }
          data = data || getExpandoData(ownerDocument);
          var clone = data.frag.cloneNode(),
          i = 0,
          elems = getElements(),
          l = elems.length;
          for(;i<l;i++){
            clone.createElement(elems[i]);
          }
          return clone;
        }

            function shivMethods(ownerDocument, data) {
          if (!data.cache) {
            data.cache = {};
            data.createElem = ownerDocument.createElement;
            data.createFrag = ownerDocument.createDocumentFragment;
            data.frag = data.createFrag();
          }


          ownerDocument.createElement = function(nodeName) {
                    if (!html5.shivMethods) {
              return data.createElem(nodeName);
            }
            return createElement(nodeName, ownerDocument, data);
          };

          ownerDocument.createDocumentFragment = Function('h,f', 'return function(){' +
                                                          'var n=f.cloneNode(),c=n.createElement;' +
                                                          'h.shivMethods&&(' +
                                                                                                                getElements().join().replace(/[\w\-]+/g, function(nodeName) {
            data.createElem(nodeName);
            data.frag.createElement(nodeName);
            return 'c("' + nodeName + '")';
          }) +
            ');return n}'
                                                         )(html5, data.frag);
        }

            function shivDocument(ownerDocument) {
          if (!ownerDocument) {
            ownerDocument = document;
          }
          var data = getExpandoData(ownerDocument);

          if (html5.shivCSS && !supportsHtml5Styles && !data.hasCSS) {
            data.hasCSS = !!addStyleSheet(ownerDocument,
                                                                                'article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}' +
                                                                                    'mark{background:#FF0;color:#000}' +
                                                                                    'template{display:none}'
                                         );
          }
          if (!supportsUnknownElements) {
            shivMethods(ownerDocument, data);
          }
          return ownerDocument;
        }

            var html5 = {

                'elements': options.elements || 'abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video',

                'version': version,

                'shivCSS': (options.shivCSS !== false),

                'supportsUnknownElements': supportsUnknownElements,

                'shivMethods': (options.shivMethods !== false),

                'type': 'default',

                'shivDocument': shivDocument,

                createElement: createElement,

                createDocumentFragment: createDocumentFragment
        };

            window.html5 = html5;

            shivDocument(document);

    }(this, document));

    Modernizr._version      = version;

    Modernizr._prefixes     = prefixes;

    Modernizr.testStyles    = injectElementWithStyles;    docElement.className = docElement.className.replace(/(^|\s)no-js(\s|$)/, '$1$2') +

                                                    (enableClasses ? ' js ' + classes.join(' ') : '');

    return Modernizr;

})(this, this.document);
// https://github.com/Modernizr/Modernizr/issues/572
// Similar to http://jsfiddle.net/FWeinb/etnYC/
Modernizr.addTest('cssvhunit', function() {
    var bool;
    Modernizr.testStyles("#modernizr { height: 50vh; }", function(elem, rule) {   
        var height = parseInt(window.innerHeight/2,10),
            compStyle = parseInt((window.getComputedStyle ?
                      getComputedStyle(elem, null) :
                      elem.currentStyle)["height"],10);
        
        bool= (compStyle == height);
    });
    return bool;
});;

if (typeof(templayed) == "undefined") {

// *
// * templayed.js 0.2.1 (Uncompressed)
// * The fastest and smallest Mustache compliant Javascript templating library written in 1806 bytes (uncompressed)
// *
// * (c) 2012 Paul Engel (Internetbureau Holder B.V.)
// * Except otherwise noted, templayed.js is licensed under
// * http://creativecommons.org/licenses/by-sa/3.0
// *
// * $Date: 2012-10-14 01:17:01 +0100 (Sun, 14 October 2012) $
// *

templayed = function(template, vars) {

  var get = function(path, i) {
    i = 1; path = path.replace(/\.\.\//g, function() { i++; return ''; });
    var js = ['vars[vars.length - ', i, ']'], keys = (path == "." ? [] : path.split(".")), j = 0;
    for (j; j < keys.length; j++) { js.push('.' + keys[j]); };
    return js.join('');
  }, tag = function(template) {
    return template.replace(/\{\{(!|&|\{)?\s*(.*?)\s*}}+/g, function(match, operator, context) {
      if (operator == "!") return '';
      var i = inc++;
      return ['"; var o', i, ' = ', get(context), ', s', i, ' = (((typeof(o', i, ') == "function" ? o', i, '.call(vars[vars.length - 1]) : o', i, ') || "") + ""); s += ',
        (operator ? ('s' + i) : '(/[&"><]/.test(s' + i + ') ? s' + i + '.replace(/&/g,"&amp;").replace(/"/g,"&quot;").replace(/>/g,"&gt;").replace(/</g,"&lt;") : s' + i + ')'), ' + "'
      ].join('');
    });
  }, block = function(template) {
    return tag(template.replace(/\{\{(\^|#)(.*?)}}(.*?)\{\{\/\2}}/g, function(match, operator, key, context) {
      var i = inc++;
      return ['"; var o', i, ' = ', get(key), '; ',
        (operator == "^" ?
          ['if ((o', i, ' instanceof Array) ? !o', i, '.length : !o', i, ') { s += "', block(context), '"; } '] :
          ['if (typeof(o', i, ') == "boolean" && o', i, ') { s += "', block(context), '"; } else if (o', i, ') { for (var i', i, ' = 0; i', i, ' < o',
            i, '.length; i', i, '++) { vars.push(o', i, '[i', i, ']); s += "', block(context), '"; vars.pop(); }}']
        ).join(''), '; s += "'].join('');
    }));
  }, inc = 0;

  return new Function("vars", 'vars = [vars], s = "' + block(template.replace(/"/g, '\\"').replace(/\n/g, '\\n')) + '"; return s;');
};

templayed.version = "0.2.1";

};

/**
 * @namespace Core
 */
var Core = (function () {
    "use strict";

    var /**
         * Register a new module to the Core object
         *
         * @function register
         * @memberOf! Core
         * @param  {string} name   Name of the module.
         * @param  {Object} module The actual module.
         * @return {Object}        The updated Core.
         * @public
         */
        register = function (name, module) {
            if (!this[name]) {
                this[name] = module;
            }

            return this;
        },

        /**
         * Empty function to use for callbacks etc. instead of having 
         * to make numerous empty functions yourself. Should save some 
         * memory.
         * 
         * @function noop
         * @memberOf! Core
         * @public
         */
        noop = function () {};

    return {
        register: register,
        noop: noop
    };
}());

/**
 * @namespace CoreSettings
 * @memberof  Core
 */
var Core = (function () {
    "use strict";

    var /**
         * The default animation duration
         *
         * @memberof! Core.CoreSettings
         * @name ANIMATION_DURATION
         * @type {number}
         * @public
         */
        ANIMATION_DURATION = 150,

        /**
         * Breakpoint reference. Should reflect those in CSS.
         *
         * @memberof! Core.CoreSettings
         * @name BREAKPOINTS
         * @type {object}
         * @public
         */
        BREAKPOINTS = {
            "x-small":    "28em",
            "small":      "33em",
            "medium":     "55em",
            "large":      "66em",
            "x-large":    "80em",
            "xx-large":  "110em",
            "xxx-large": "130em"
        },

        /**
         * Return a Media Query string
         *
         * @function mediaQuery
         * @memberof! Core.CoreSettings
         * @public
         * @param {string} breakpoint A breakpoint from the list above
         * @param {bool} min Whether to use min-width or max-width. Default: min-width.
         * @return {string} The complete media query string.
         */
        mediaQuery = function (breakpoint, min) {
            var mobileFirst = min === false ? "max" : "min";

            return "screen and (" + mobileFirst + "-width: " + breakpoint + ")";
        };

    return Core.register("Settings", {
        ANIMATION_DURATION: ANIMATION_DURATION,
        BREAKPOINTS: BREAKPOINTS,
        mediaQuery: mediaQuery
    });
}(Core || {}));

/**
 * @namespace Analytics
 * @memberOf  Core
 *
 * @todo  Finish implementation
 * @todo  Check implementation with Paul
 */
var Core = (function (Core, ga) {
    "use strict";

    var _funnelAttr = "funnel",
        _funnelSelector = "[data-" + _funnelAttr + "]",
        _funnelUrlPrefix = "trechters/",

        _trackPageview = function () {
            var hasAttrs = $(this).attr(_funnelAttr),
                attrs;

            if (!hasAttrs) {
                return;
            }

            attrs = hasAttrs.split(",");

            ga("send", {
                "hitType": "event",
                "eventCategory": "modal",
                "eventAction": "open",
                "eventLabel": "header-usps"
            });
        },

        setup = function () {
            $(_funnelSelector).on("click", _trackPageview);
        };

    return Core.register("Analytics", {
        setup: setup
    });
}(Core || {}, ga));

/**
 * @namespace ContentAppender
 * @memberOf  Core
 */
var Core = (function (Core, enquire, templayed) {
    "use strict";

    var /**
         * Data object representing the content to be appended
         *
         * @memberOf! Core.ContentAppender
         * @name _content
         * @type {Object}
         * @private
         */
        _content,

        /**
         * Template string to be compiled into HTML
         *
         * @memberOf! Core.ContentAppender
         * @name _template
         * @type {String}
         * @private
         */
        _template,

        /**
         * The element that will have the new content appended to
         *
         * @memberOf! Core.ContentAppender
         * @name _context
         * @type {HTMLElement}
         * @private
         */
        _context,

        /**
         * 'Only add our new content once'-flag
         *
         * @memberOf! Core.ContentAppender
         * @name _contentAdded
         * @type {Boolean}
         * @private
         */
        _contentAdded,

        /**
         * Add the content to the DOM by compiling the templayed template.
         *
         * @function addContent
         * @memberOf! Core.ContentAppender
         * @return {Bool} Whether content was added or not.
         * @public
         */
        addContent = function () {

            // Only add content once
            if (_contentAdded) {
                return false;
            }

            // Compile here, to prevent overhead.
            _context.innerHTML += templayed(_template.innerHTML)(_content);
            _contentAdded = true;

            return _contentAdded;
        },

        /**
         * Setup our DOM elements and content.
         *
         * @function append
         * @memberOf! Core.ContentAppender
         * @param {Object} options All options
         * @public
         */
        append = function (options) {
            _context = options.context;
            _template = options.template;
            _content = options.content;
            _contentAdded = false;

            enquire.register(Core.Settings.mediaQuery(Core.Settings.BREAKPOINTS.small), {
                match: addContent
            });
        },

        /**
         * Empty setup
         *
         * @function setup
         * @memberOf! Core.ContentAppender
         * @public
         */
        setup = Core.noop;

    return Core.register("ContentAppender", {
        setup: setup,
        append: append,
        addContent: addContent
    });
}(Core || {}, enquire, templayed));

/**
 * @namespace ContentArranger
 * @memberOf  Core
 */
var Core = (function (Core, document, enquire, $) {
    "use strict";

    var /**
         * Contains initial flowing content
         *
         * @name _content
         * @memberOf! Core.ContentArranger
         * @type {HTMLElement}
         * @private
         */
        _content,

        /**
         * Flowing content will flow into this
         *
         * @name _sidebar
         * @memberOf! Core.ContentArranger
         * @type {HTMLElement}
         * @private
         */
        _sidebar,

        /**
         * Flowing content elements
         *
         * @name _items
         * @memberOf! Core.ContentArranger
         * @type {Array<HTMLElement>}
         * @private
         */
        _items,

        /**
         * Remove a node from it's parent and append it to the 
         * designated container.
         *
         * @function _moveNode
         * @memberOf! Core.ContentArranger
         * @param  {HTMLElement} node The node to move around.
         * @param  {HTMLElement} into The designated destination node.
         * @return {HTMLElement}      The destination node into which the node
         *                            has been placed.
         * @private
         */
        _moveNode = function (node, into) {
            if (into === null) {
                return false;
            }

            return into.appendChild(node.parentNode.removeChild(node));
        },

        /**
         * Flow items into the designated container/sidebar/whatever.
         *
         * @function expand
         * @memberOf! Core.ContentArranger
         * @return {HTMLElement} The element where items have flown into
         * @public
         */
        expand = function () {
            var frag = document.createDocumentFragment(),
                items = _items(_content),
                size = items.length,
                i;

            for (i = 0; i < size; i += 1) {
                _moveNode(items[i], frag);
            }

            return _sidebar.appendChild(frag);
        },

        /**
         * Flow items back into the initial container.
         *
         * @function collapse
         * @memberOf! Core.ContentArranger
         * @public
         */
        collapse = function () {
            var items = _items(_sidebar),
                size = items.length,
                item, i;

            for (i = 0; i < size; i+= 1) {
                item = items[i];
                _moveNode(item, document.getElementById(item.getAttribute("id") + "cont"));
            }
        },

        /**
         * Setup DOM elements.
         *
         * @function setup
         * @memberOf! Core.ContentArranger
         * @public
         */
        setup = function () {
            _content = document.getElementById("PageContent");
            _sidebar = document.getElementById("PageSideBar");
            _items = function (context) {
                return $(context).find(".PageSide");
            };

            enquire.register(Core.Settings.mediaQuery(Core.Settings.BREAKPOINTS.medium, false), {
                match: collapse,
                unmatch: expand
            });
        };

    return Core.register("ContentArranger", {
        setup: setup,
        expand: expand,
        collapse: collapse
    });
}(Core || {}, document, enquire, jQuery));

/**
 * @namespace CSSInjector
 * @memberOf  Core
 */
var Core = (function (Core, document) {
    "use strict";

    var /**
          * Reference to a document's dynamically inserted stylesheet
          * to which new rules can be applied.
          *
          * @name _sheet
          * @memberOf! Core.CSSInjector
          * @type {sheet}
          * @private
          */
        _sheet,

        /**
          * Inject method that puts new CSS in the document head.
          *
          * @function inject
          * @memberOf! Core.CSSInjector
          * @param selector {string} The CSS selector
          * @param declarations {string|array} A String for a single CSS declaration
          *                                    or an Array for multiple
          * @note Add support for IE9- by using _sheet.addRule(selector, declarations);
          * @public
          */
        inject = function (selector, declarations) {
            // Put a semicolon in between declarations if there are multiple (Array)
            declarations = declarations instanceof Array ? declarations.join(";") : declarations;
            
            _sheet.insertRule(selector + "{" + declarations + "}", 0);
        },

        /**
         * Setup this module
         *
         * @function setup
         * @memberOf! Core.CSSInjector
         * @public
         */
        setup = function () {
            // Not quite readable, but it creates a <style> element, puts 
            // it in <head> and fetches the sheet.
            _sheet = document.head.appendChild(document.createElement("style")).sheet;
        };

    return Core.register("CSSInjector", {
        inject: inject,
        setup: setup
    });
}(Core || {}, document));

/**
 * @namespace DOMHelper
 * @memberOf  Core
 */
var Core = (function (Core) {
    "use strict";

    var /**
         * Check if an element's inner dimensions are larger than it's outer.
         *
         * @function elementhasOverflow
         * @memberOf! Core.DOMHelper
         * @param  {HTMLElement} element The element that should be checked.
         * @return {bool}        True if inner dimensions are larger than outer
         * @public
         */
        elementHasOverflow = function (element) {
            var currentOverflow = element.style.overflow,
                isOverflowing;

            // Switch current visibility settings to force possible overflow
            if (!currentOverflow || currentOverflow === "visible") {
                element.style.overflow = "hidden";
            }

            // The actual overflow check
            isOverflowing = (element.clientWidth < element.scrollWidth) || (element.clientHeight < element.scrollHeight);

            // Reset to initial visibility
            element.style.overflow = currentOverflow;

            return isOverflowing;
        },

        /**
         * Setup this module
         *
         * @function setup
         * @memberOf! Core.DOMHelper
         * @public
         */
        setup = Core.noop;

    return Core.register("DOMHelper", {
        setup: setup,
        elementHasOverflow: elementHasOverflow
    });
}(Core || {}));

/**
 * @namespace Mobilenav
 * @memberOf  Core
 */
var Core = (function (Core, document, $, enquire) {
    "use strict";

    var /**
         * The element to which the scroll position is compared
         *
         * @name _element
         * @memberOf! Core.MobileNav
         * @type {HTMLElement}
         * @private
         */
        _element,

        /**
         * The mobile navigation
         *
         * @name _navElement
         * @memberOf! Core.MobileNav
         * @type {HTMLElement}
         * @private
         */
        _navElement,

        /**
         * The toggle button for the mobile navigation
         *
         * @name _toggler
         * @memberOf! Core.MobileNav
         * @type {HTMLElement}
         * @private
         */
        _toggler,

        /**
         * The window element, cached
         *
         * @name _window
         * @memberOf! Core.MobileNav
         * @type {HTMLElement}
         * @private
         */
        _window,

        /**
         * Calculate and set the maximum height of the Mobile Navigation.
         *
         * @function _setMaxHeigth
         * @memberOf! Core.MobileNav
         * @return {Number} The height in pixels.
         * @private
         */
        _setMaxHeight = function () {
            var windowHeight = parseInt(_window.height(), 10),
                pageHeadHeight = parseInt($("#PageHead").height(), 10),
                uspHeight = parseInt($("#PageHeadUsp").height(), 10),
                maxHeight = windowHeight - pageHeadHeight + uspHeight;

            Core.CSSInjector.inject(".MobileNav__Nav--IsVisible", "max-height: " + maxHeight + "px");
            return maxHeight;
        },

        /**
         * Calculates position of the current window and the navigation
         * to apply a class to stick the element.
         *
         * @function _applyStick
         * @memberOf! Core.MobileNav
         * @private
         */
        _applyStick = function () {
            var stickyClass = "MobileNav--IsSticky";

            if (_element.get(0).offsetTop < window.scrollY) {
                _element.addClass(stickyClass);
            } else {
                _element.removeClass(stickyClass);
            }
        },

        /**
         * Toggles the button and navigation state of the Mobile Navigation.
         *
         * @function _toggle
         * @memberOf! Core.MobileNav
         * @private
         */
        _toggle = function () {
            _toggler.toggleClass("MobileNav__Toggle--IsOpen");
            _navElement.toggleClass("MobileNav__Nav--IsVisible");
        },

        /**
         * Attach event handlers.
         *
         * @function _attachEvents
         * @memberOf! Core.MobileNav
         * @private
         */
        _attachEvents = function () {
            _toggler.on("click", _toggle);
            _window.on("resize", _setMaxHeight);
            _window.on("scroll", _applyStick);
        },

        /**
         * Setup module: register DOM elements and initiate inner methods.
         * Only run on small devices that have the mobile nav enabled.
         *
         * @function setup
         * @memberOf! Core.MobileNav
         * @public
         */
        setup = function () {
            enquire.register(Core.Settings.mediaQuery(Core.Settings.BREAKPOINTS.medium, false), {
                match: function () {
                    _element = $(document.getElementById("MobileNav"));
                    _toggler = $(document.getElementById("MobileNavToggle"));
                    _navElement = $(document.getElementById("MobileNavNav"));
                    _window = $(window);

                    _setMaxHeight();
                    _applyStick();
                    _attachEvents();
                }
            });
        };

    return Core.register("Mobilenav", {
        setup: setup
    });
}(Core || {}, document, jQuery, enquire));

/**
 * @namespace Modal
 * @memberOf  Core
 */
var Core = (function (Core, document, $, Modernizr) {
    "use strict";

    var /**
         * The semi transparent Modal backdrop/background
         *
         * @name _backdrop
         * @memberOf! Core.Modal
         * @type {HTMLElement}
         * @private
         */
        _backdrop,

        /**
         * The actual Modal element
         *
         * @name _modal
         * @memberOf! Core.Modal
         * @type {HTMLElement}
         * @private
         */
        _modal,

        /**
         * ClassName for the active modal
         *
         * @name _modalStateClass
         * @memberOf! Core.Modal
         * @type {string}
         * @private
         */
        _modalStateClass = "Modal--Active",

        /**
         * ClassName for the active backdrop
         *
         * @name _backdropStateClass
         * @memberOf! Core.Modal
         * @type {string}
         * @private
         */
        _backdropStateClass = "Modal__Backdrop--Active",

        /**
         * ClassName for the Modal body
         *
         * @name _modalBodyClass
         * @memberOf Core.Modal
         * @type {string}
         * @private
         */
        _modalBodyClass = "Modal__Body",

        /**
         * The maximum height of the modal relative to the viewport
         * Corresponds to (_maxHeight * 10)vh in CSS
         *
         * @name _maxHeight
         * @memberOf! Core.Modal
         * @type {number}
         * @private
         */
        _maxHeight = 0.8,

        /**
         * Cached jQuery-wrapped document
         *
         * @name _document
         * @memberOf Core.Modal
         * @type {Object}
         * @private
         */
        _document = $(document),

        /**
         * Cached jQuery-wrapped window
         *
         * @name _window
         * @memberOf Core.Modal
         * @type {Object}
         * @private
         */
        _window = $(window),

        /**
         * Set the modal body height for browsers that don't support
         * the vh unit.
         *
         * @function _setBodyHeight
         * @memberOf! Core.Modal
         * @private
         */
        _setBodyHeight = function () {
            Core.CSSInjector.inject("." + _modalBodyClass, "max-height: " + _window.height() * _maxHeight + "px");
        },

        /**
         * Method to run after a '.js-togglemodal' was clicked.
         *
         * @function _open
         * @memberOf! Core.Modal
         * @private
         */
        _open = function (event) {
            event.preventDefault();

            // Set the modal, according to the button that triggered it.
            _modal = $(document.getElementById(this.getAttribute("data-modal")));

            // Show the modal
            _modal.addClass(_modalStateClass);
            _backdrop.addClass(_backdropStateClass);

            // Set the inner body height of the modal
            if (!Modernizr.cssvhunit) {
                _setBodyHeight();
            }

            // Attach events to close the modal
            _document.on("keyup", _closeOnKey);
            $(".Modal__Close").on("click", _close);
            _backdrop.on("click", _close);
        },

        /**
         * Hide the current modal and backdrop.
         *
         * @function _close
         * @memberOf! Core.Modal
         * @private
         */
        _close = function () {
            _modal.removeClass(_modalStateClass);
            _backdrop.removeClass(_backdropStateClass);

            // Clean up events to close the modal
            _document.off("keyup", _closeOnKey);
            $(".Modal__Close").off("click", _close);
            _backdrop.off("click", _close);
        },

        /**
         * Close the modal with a keypress. Needs to be cached to event
         * handlers can be toggled on this function.
         *
         * @function _closeOnKey
         * @memberOf Core.Modal
         * @param  {event} event Any keypress event
         * @private
         */
        _closeOnKey = function (event) {
            if (event.keyCode === 27) {
                _close();
            }
        },

        /**
         * Attach handler to modal togglers. Also adds a
         * resize handler if CSS vh unit is not supported.
         *
         * @function _attachEvents
         * @memberOf! Core.Modal
         * @private
         */
        _attachEvents = function () {
            $(".js-togglemodal").on("click", _open);

            if (!Modernizr.cssvhunit) {
                _window.on("resize", _setBodyHeight);
            }
        },

        /**
         * Create a modal backdrop and add it to the page.
         *
         * @function _createBackdrop
         * @memberOf! Core.Modal
         * @return {HTMLElement} The backdrop element
         * @private
         */
        _createBackdrop = function () {
            _backdrop = document.createElement("div");
            _backdrop.id = "ModalBackdrop";
            _backdrop.className = "Modal__Backdrop";

            // Insert backdrop in document
            document.body.appendChild(_backdrop);

            // Cache the backdrop as jQuery-object, as it will only be
            // used as jQuery-object to toggle classes.
            _backdrop = $(_backdrop);
        },

        /**
         * Setup this module
         *
         * @function setup
         * @memberOf! Core.Modal
         * @public
         */
        setup = function () {
            _createBackdrop();
            _attachEvents();
        };

    return Core.register("Modal", {
        setup: setup
    });
}(Core || {}, document, jQuery, Modernizr));

/**
 * @namespace Selector
 * @memberOf  Core
 */
var Core = (function (Core, $) {
    "use strict";

    var /**
         * Set disabled states to price table buttons if the row they
         * are in is not showing a scrollbar
         *
         * @function _setPriceTableButtons
         * @memberOf! Core.Selector
         * @private
         */
        _setPriceTableButtons = function () {
            var rows = $("#SelectorPriceTable").find(".PriceTable__hScroller"),
                btns;

            // Loop through the rows, check if there's overflow, set disabled
            // styles accordingly.
            rows.each(function (index, row) {
                if (!Core.DOMHelper.elementHasOverflow(row)) {
                    $(row).find(".PriceTable__Btn").attr("disabled", "disabled");
                }
            });
        },

        /**
         * Attach event handlers
         *
         * @function _attachEvents
         * @memberOf! Core.Selector
         * @private
         */
        _attachEvents = function () {
            $(window).on("resize", _setPriceTableButtons);
        },

        /**
         * Setup this module
         *
         * @function setup
         * @memberOf! Core.Selector
         * @public
         */
        setup = function () {
            _setPriceTableButtons();
            _attachEvents();
        };

    return Core.register("Selector", {
        setup: setup
    });
}(Core || {}, jQuery));

/**
 * @namespace StackEqualizer
 * @memberof  Core
 */
var Core = (function (Core, $) {
    "use strict";

    var /**
         * The element in which the elements should be equalized
         *
         * @name _context
         * @memberOf! Core.StackEqualizer
         * @type {jQuery object}
         * @private
         */
        _context,

        /**
          * Elements inside the context that should be equalized
         *
         * @name _elements
         * @memberOf! Core.StackEqualizer
         * @type {functoin}
         * @private
          */
        _elements,

        /**
         * Normalize column heights
         *
         * @function _normalize
         * @memberOf! Core.StackEqualizer
         * @private
         */
        _normalize = function () {
            _context.each(function () {
                $(_elements, this).css("height", "auto");
            });
        },

        /**
         * Create equal height columns in a container
         *
         * @function equalize
         * @memberOf! Core.StackEqualizer
         * @param {object} context The element which columns need equalizing.
         * @param {function} selector Elements which need equalizing.
         * @public
         *
         * @fixme Returns erroneous results because images aren't always loaded in time.
         */
        equalize = function (context, selector) {
            var heights,
                items,
                tallest;

            context.each(function () {
                tallest = 0;

                // Get the items with the current context
                items = selector.apply($(this));

                // Create an array with all heights
                heights = items.map(function () {
                    return parseInt(this.scrollHeight, 10);
                });

                tallest = Math.max.apply(null, heights);

                // Set the actual height after calculating it
                items.css("height", tallest + "px");
            });
        },

        /**
         * Empty setup
         *
         * @function setup
         * @memberOf! Core.StackEqualizer
         * @public
         */
        setup = Core.noop;

    /**
     * Public API
     */
    return Core.register("StackEqualizer", {
        setup: setup,
        equalize: equalize
    });
}(Core || {}, jQuery));

/*global Core*/
(function (Core, $, Echo) {
    "use strict";

    // Run all modules from a single for-loop
    var module;

    for (module in Core) {
        // Check if modules exist and if they have a setup method
        if (Core.hasOwnProperty(module) && Core[module].hasOwnProperty("setup")) {
            Core[module].setup();
        }
    }

    /*****************************************************************
     * GENERAL METHODS USED THROUGHOUT SITE                          *
     ****************************************************************/
    Core.StackEqualizer.equalize($(".Order"), function () {
        return this.find(".Order__Value");
    });

    // @note Disabled until issue with image loading is fixed.
    // Core.StackEqualizer.equalize($(".Matrix"), function () {
    //    return this.children("li");
    // });

    // @note Core.ContentAppender is called from DOM directly

    // Apply lazy loading for images
    Echo.init();

    /*****************************************************************
     * ACCOUNT                                                       *
     ****************************************************************/
    $(".OrderRulesToggler").on("click", function () {
        $(this).toggleClass("OrderRulesToggler--IsOpen").prev().slideToggle(Core.Settings.ANIMATION_DURATION);
    });

}(Core, jQuery, Echo));
