/**
 * @namespace Analytics
 * @memberOf  Core
 *
 * @todo  Finish implementation
 * @todo  Check implementation with Paul
 */
var Core = (function (Core, ga) {
    "use strict";

    var _funnelAttr = "funnel",
        _funnelSelector = "[data-" + _funnelAttr + "]",
        _funnelUrlPrefix = "trechters/",

        _trackPageview = function () {
            var hasAttrs = $(this).attr(_funnelAttr),
                attrs;

            if (!hasAttrs) {
                return;
            }

            attrs = hasAttrs.split(",");

            ga("send", {
                "hitType": "event",
                "eventCategory": "modal",
                "eventAction": "open",
                "eventLabel": "header-usps"
            });
        },

        setup = function () {
            $(_funnelSelector).on("click", _trackPageview);
        };

    return Core.register("Analytics", {
        setup: setup
    });
}(Core || {}, ga));
