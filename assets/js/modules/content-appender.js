/**
 * @namespace ContentAppender
 * @memberOf  Core
 */
var Core = (function (Core, enquire, templayed) {
    "use strict";

    var /**
         * Data object representing the content to be appended
         *
         * @memberOf! Core.ContentAppender
         * @name _content
         * @type {Object}
         * @private
         */
        _content,

        /**
         * Template string to be compiled into HTML
         *
         * @memberOf! Core.ContentAppender
         * @name _template
         * @type {String}
         * @private
         */
        _template,

        /**
         * The element that will have the new content appended to
         *
         * @memberOf! Core.ContentAppender
         * @name _context
         * @type {HTMLElement}
         * @private
         */
        _context,

        /**
         * 'Only add our new content once'-flag
         *
         * @memberOf! Core.ContentAppender
         * @name _contentAdded
         * @type {Boolean}
         * @private
         */
        _contentAdded,

        /**
         * Add the content to the DOM by compiling the templayed template.
         *
         * @function addContent
         * @memberOf! Core.ContentAppender
         * @return {Bool} Whether content was added or not.
         * @public
         */
        addContent = function () {

            // Only add content once
            if (_contentAdded) {
                return false;
            }

            // Compile here, to prevent overhead.
            _context.innerHTML += templayed(_template.innerHTML)(_content);
            _contentAdded = true;

            return _contentAdded;
        },

        /**
         * Setup our DOM elements and content.
         *
         * @function append
         * @memberOf! Core.ContentAppender
         * @param {Object} options All options
         * @public
         */
        append = function (options) {
            _context = options.context;
            _template = options.template;
            _content = options.content;
            _contentAdded = false;

            enquire.register(Core.Settings.mediaQuery(Core.Settings.BREAKPOINTS.small), {
                match: addContent
            });
        },

        /**
         * Empty setup
         *
         * @function setup
         * @memberOf! Core.ContentAppender
         * @public
         */
        setup = Core.noop;

    return Core.register("ContentAppender", {
        setup: setup,
        append: append,
        addContent: addContent
    });
}(Core || {}, enquire, templayed));
