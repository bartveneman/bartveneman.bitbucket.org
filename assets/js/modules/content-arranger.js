/**
 * @namespace ContentArranger
 * @memberOf  Core
 */
var Core = (function (Core, document, enquire, $) {
    "use strict";

    var /**
         * Contains initial flowing content
         *
         * @name _content
         * @memberOf! Core.ContentArranger
         * @type {HTMLElement}
         * @private
         */
        _content,

        /**
         * Flowing content will flow into this
         *
         * @name _sidebar
         * @memberOf! Core.ContentArranger
         * @type {HTMLElement}
         * @private
         */
        _sidebar,

        /**
         * Flowing content elements
         *
         * @name _items
         * @memberOf! Core.ContentArranger
         * @type {Array<HTMLElement>}
         * @private
         */
        _items,

        /**
         * Remove a node from it's parent and append it to the 
         * designated container.
         *
         * @function _moveNode
         * @memberOf! Core.ContentArranger
         * @param  {HTMLElement} node The node to move around.
         * @param  {HTMLElement} into The designated destination node.
         * @return {HTMLElement}      The destination node into which the node
         *                            has been placed.
         * @private
         */
        _moveNode = function (node, into) {
            if (into === null) {
                return false;
            }

            return into.appendChild(node.parentNode.removeChild(node));
        },

        /**
         * Flow items into the designated container/sidebar/whatever.
         *
         * @function expand
         * @memberOf! Core.ContentArranger
         * @return {HTMLElement} The element where items have flown into
         * @public
         */
        expand = function () {
            var frag = document.createDocumentFragment(),
                items = _items(_content),
                size = items.length,
                i;

            for (i = 0; i < size; i += 1) {
                _moveNode(items[i], frag);
            }

            return _sidebar.appendChild(frag);
        },

        /**
         * Flow items back into the initial container.
         *
         * @function collapse
         * @memberOf! Core.ContentArranger
         * @public
         */
        collapse = function () {
            var items = _items(_sidebar),
                size = items.length,
                item, i;

            for (i = 0; i < size; i+= 1) {
                item = items[i];
                _moveNode(item, document.getElementById(item.getAttribute("id") + "cont"));
            }
        },

        /**
         * Setup DOM elements.
         *
         * @function setup
         * @memberOf! Core.ContentArranger
         * @public
         */
        setup = function () {
            _content = document.getElementById("PageContent");
            _sidebar = document.getElementById("PageSideBar");
            _items = function (context) {
                return $(context).find(".PageSide");
            };

            enquire.register(Core.Settings.mediaQuery(Core.Settings.BREAKPOINTS.medium, false), {
                match: collapse,
                unmatch: expand
            });
        };

    return Core.register("ContentArranger", {
        setup: setup,
        expand: expand,
        collapse: collapse
    });
}(Core || {}, document, enquire, jQuery));
