/**
 * @namespace CSSInjector
 * @memberOf  Core
 */
var Core = (function (Core, document) {
    "use strict";

    var /**
          * Reference to a document's dynamically inserted stylesheet
          * to which new rules can be applied.
          *
          * @name _sheet
          * @memberOf! Core.CSSInjector
          * @type {sheet}
          * @private
          */
        _sheet,

        /**
          * Inject method that puts new CSS in the document head.
          *
          * @function inject
          * @memberOf! Core.CSSInjector
          * @param selector {string} The CSS selector
          * @param declarations {string|array} A String for a single CSS declaration
          *                                    or an Array for multiple
          * @note Add support for IE9- by using _sheet.addRule(selector, declarations);
          * @public
          */
        inject = function (selector, declarations) {
            // Put a semicolon in between declarations if there are multiple (Array)
            declarations = declarations instanceof Array ? declarations.join(";") : declarations;
            
            _sheet.insertRule(selector + "{" + declarations + "}", 0);
        },

        /**
         * Setup this module
         *
         * @function setup
         * @memberOf! Core.CSSInjector
         * @public
         */
        setup = function () {
            // Not quite readable, but it creates a <style> element, puts 
            // it in <head> and fetches the sheet.
            _sheet = document.head.appendChild(document.createElement("style")).sheet;
        };

    return Core.register("CSSInjector", {
        inject: inject,
        setup: setup
    });
}(Core || {}, document));
