/**
 * @namespace DOMHelper
 * @memberOf  Core
 */
var Core = (function (Core) {
    "use strict";

    var /**
         * Check if an element's inner dimensions are larger than it's outer.
         *
         * @function elementhasOverflow
         * @memberOf! Core.DOMHelper
         * @param  {HTMLElement} element The element that should be checked.
         * @return {bool}        True if inner dimensions are larger than outer
         * @public
         */
        elementHasOverflow = function (element) {
            var currentOverflow = element.style.overflow,
                isOverflowing;

            // Switch current visibility settings to force possible overflow
            if (!currentOverflow || currentOverflow === "visible") {
                element.style.overflow = "hidden";
            }

            // The actual overflow check
            isOverflowing = (element.clientWidth < element.scrollWidth) || (element.clientHeight < element.scrollHeight);

            // Reset to initial visibility
            element.style.overflow = currentOverflow;

            return isOverflowing;
        },

        /**
         * Setup this module
         *
         * @function setup
         * @memberOf! Core.DOMHelper
         * @public
         */
        setup = Core.noop;

    return Core.register("DOMHelper", {
        setup: setup,
        elementHasOverflow: elementHasOverflow
    });
}(Core || {}));
