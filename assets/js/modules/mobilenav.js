/**
 * @namespace Mobilenav
 * @memberOf  Core
 */
var Core = (function (Core, document, $, enquire) {
    "use strict";

    var /**
         * The element to which the scroll position is compared
         *
         * @name _element
         * @memberOf! Core.MobileNav
         * @type {HTMLElement}
         * @private
         */
        _element,

        /**
         * The mobile navigation
         *
         * @name _navElement
         * @memberOf! Core.MobileNav
         * @type {HTMLElement}
         * @private
         */
        _navElement,

        /**
         * The toggle button for the mobile navigation
         *
         * @name _toggler
         * @memberOf! Core.MobileNav
         * @type {HTMLElement}
         * @private
         */
        _toggler,

        /**
         * The window element, cached
         *
         * @name _window
         * @memberOf! Core.MobileNav
         * @type {HTMLElement}
         * @private
         */
        _window,

        /**
         * Calculate and set the maximum height of the Mobile Navigation.
         *
         * @function _setMaxHeigth
         * @memberOf! Core.MobileNav
         * @return {Number} The height in pixels.
         * @private
         */
        _setMaxHeight = function () {
            var windowHeight = parseInt(_window.height(), 10),
                pageHeadHeight = parseInt($("#PageHead").height(), 10),
                uspHeight = parseInt($("#PageHeadUsp").height(), 10),
                maxHeight = windowHeight - pageHeadHeight + uspHeight;

            Core.CSSInjector.inject(".MobileNav__Nav--IsVisible", "max-height: " + maxHeight + "px");
            return maxHeight;
        },

        /**
         * Calculates position of the current window and the navigation
         * to apply a class to stick the element.
         *
         * @function _applyStick
         * @memberOf! Core.MobileNav
         * @private
         */
        _applyStick = function () {
            var stickyClass = "MobileNav--IsSticky";

            if (_element.get(0).offsetTop < window.scrollY) {
                _element.addClass(stickyClass);
            } else {
                _element.removeClass(stickyClass);
            }
        },

        /**
         * Toggles the button and navigation state of the Mobile Navigation.
         *
         * @function _toggle
         * @memberOf! Core.MobileNav
         * @private
         */
        _toggle = function () {
            _toggler.toggleClass("MobileNav__Toggle--IsOpen");
            _navElement.toggleClass("MobileNav__Nav--IsVisible");
        },

        /**
         * Attach event handlers.
         *
         * @function _attachEvents
         * @memberOf! Core.MobileNav
         * @private
         */
        _attachEvents = function () {
            _toggler.on("click", _toggle);
            _window.on("resize", _setMaxHeight);
            _window.on("scroll", _applyStick);
        },

        /**
         * Setup module: register DOM elements and initiate inner methods.
         * Only run on small devices that have the mobile nav enabled.
         *
         * @function setup
         * @memberOf! Core.MobileNav
         * @public
         */
        setup = function () {
            enquire.register(Core.Settings.mediaQuery(Core.Settings.BREAKPOINTS.medium, false), {
                match: function () {
                    _element = $(document.getElementById("MobileNav"));
                    _toggler = $(document.getElementById("MobileNavToggle"));
                    _navElement = $(document.getElementById("MobileNavNav"));
                    _window = $(window);

                    _setMaxHeight();
                    _applyStick();
                    _attachEvents();
                }
            });
        };

    return Core.register("Mobilenav", {
        setup: setup
    });
}(Core || {}, document, jQuery, enquire));
