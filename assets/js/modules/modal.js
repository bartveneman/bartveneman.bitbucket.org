/**
 * @namespace Modal
 * @memberOf  Core
 */
var Core = (function (Core, document, $, Modernizr) {
    "use strict";

    var /**
         * The semi transparent Modal backdrop/background
         *
         * @name _backdrop
         * @memberOf! Core.Modal
         * @type {HTMLElement}
         * @private
         */
        _backdrop,

        /**
         * The actual Modal element
         *
         * @name _modal
         * @memberOf! Core.Modal
         * @type {HTMLElement}
         * @private
         */
        _modal,

        /**
         * ClassName for the active modal
         *
         * @name _modalStateClass
         * @memberOf! Core.Modal
         * @type {string}
         * @private
         */
        _modalStateClass = "Modal--Active",

        /**
         * ClassName for the active backdrop
         *
         * @name _backdropStateClass
         * @memberOf! Core.Modal
         * @type {string}
         * @private
         */
        _backdropStateClass = "Modal__Backdrop--Active",

        /**
         * ClassName for the Modal body
         *
         * @name _modalBodyClass
         * @memberOf Core.Modal
         * @type {string}
         * @private
         */
        _modalBodyClass = "Modal__Body",

        /**
         * The maximum height of the modal relative to the viewport
         * Corresponds to (_maxHeight * 10)vh in CSS
         *
         * @name _maxHeight
         * @memberOf! Core.Modal
         * @type {number}
         * @private
         */
        _maxHeight = 0.8,

        /**
         * Cached jQuery-wrapped document
         *
         * @name _document
         * @memberOf Core.Modal
         * @type {Object}
         * @private
         */
        _document = $(document),

        /**
         * Cached jQuery-wrapped window
         *
         * @name _window
         * @memberOf Core.Modal
         * @type {Object}
         * @private
         */
        _window = $(window),

        /**
         * Set the modal body height for browsers that don't support
         * the vh unit.
         *
         * @function _setBodyHeight
         * @memberOf! Core.Modal
         * @private
         */
        _setBodyHeight = function () {
            Core.CSSInjector.inject("." + _modalBodyClass, "max-height: " + _window.height() * _maxHeight + "px");
        },

        /**
         * Method to run after a '.js-togglemodal' was clicked.
         *
         * @function _open
         * @memberOf! Core.Modal
         * @private
         */
        _open = function (event) {
            event.preventDefault();

            // Set the modal, according to the button that triggered it.
            _modal = $(document.getElementById(this.getAttribute("data-modal")));

            // Show the modal
            _modal.addClass(_modalStateClass);
            _backdrop.addClass(_backdropStateClass);

            // Set the inner body height of the modal
            if (!Modernizr.cssvhunit) {
                _setBodyHeight();
            }

            // Attach events to close the modal
            _document.on("keyup", _closeOnKey);
            $(".Modal__Close").on("click", _close);
            _backdrop.on("click", _close);
        },

        /**
         * Hide the current modal and backdrop.
         *
         * @function _close
         * @memberOf! Core.Modal
         * @private
         */
        _close = function () {
            _modal.removeClass(_modalStateClass);
            _backdrop.removeClass(_backdropStateClass);

            // Clean up events to close the modal
            _document.off("keyup", _closeOnKey);
            $(".Modal__Close").off("click", _close);
            _backdrop.off("click", _close);
        },

        /**
         * Close the modal with a keypress. Needs to be cached to event
         * handlers can be toggled on this function.
         *
         * @function _closeOnKey
         * @memberOf Core.Modal
         * @param  {event} event Any keypress event
         * @private
         */
        _closeOnKey = function (event) {
            if (event.keyCode === 27) {
                _close();
            }
        },

        /**
         * Attach handler to modal togglers. Also adds a
         * resize handler if CSS vh unit is not supported.
         *
         * @function _attachEvents
         * @memberOf! Core.Modal
         * @private
         */
        _attachEvents = function () {
            $(".js-togglemodal").on("click", _open);

            if (!Modernizr.cssvhunit) {
                _window.on("resize", _setBodyHeight);
            }
        },

        /**
         * Create a modal backdrop and add it to the page.
         *
         * @function _createBackdrop
         * @memberOf! Core.Modal
         * @return {HTMLElement} The backdrop element
         * @private
         */
        _createBackdrop = function () {
            _backdrop = document.createElement("div");
            _backdrop.id = "ModalBackdrop";
            _backdrop.className = "Modal__Backdrop";

            // Insert backdrop in document
            document.body.appendChild(_backdrop);

            // Cache the backdrop as jQuery-object, as it will only be
            // used as jQuery-object to toggle classes.
            _backdrop = $(_backdrop);
        },

        /**
         * Setup this module
         *
         * @function setup
         * @memberOf! Core.Modal
         * @public
         */
        setup = function () {
            _createBackdrop();
            _attachEvents();
        };

    return Core.register("Modal", {
        setup: setup
    });
}(Core || {}, document, jQuery, Modernizr));
