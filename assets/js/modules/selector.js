/**
 * @namespace Selector
 * @memberOf  Core
 */
var Core = (function (Core, $) {
    "use strict";

    var /**
         * Set disabled states to price table buttons if the row they
         * are in is not showing a scrollbar
         *
         * @function _setPriceTableButtons
         * @memberOf! Core.Selector
         * @private
         */
        _setPriceTableButtons = function () {
            var rows = $("#SelectorPriceTable").find(".PriceTable__hScroller"),
                btns;

            // Loop through the rows, check if there's overflow, set disabled
            // styles accordingly.
            rows.each(function (index, row) {
                if (!Core.DOMHelper.elementHasOverflow(row)) {
                    $(row).find(".PriceTable__Btn").attr("disabled", "disabled");
                }
            });
        },

        /**
         * Attach event handlers
         *
         * @function _attachEvents
         * @memberOf! Core.Selector
         * @private
         */
        _attachEvents = function () {
            $(window).on("resize", _setPriceTableButtons);
        },

        /**
         * Setup this module
         *
         * @function setup
         * @memberOf! Core.Selector
         * @public
         */
        setup = function () {
            _setPriceTableButtons();
            _attachEvents();
        };

    return Core.register("Selector", {
        setup: setup
    });
}(Core || {}, jQuery));
