/**
 * @namespace StackEqualizer
 * @memberof  Core
 */
var Core = (function (Core, $) {
    "use strict";

    var /**
         * The element in which the elements should be equalized
         *
         * @name _context
         * @memberOf! Core.StackEqualizer
         * @type {jQuery object}
         * @private
         */
        _context,

        /**
          * Elements inside the context that should be equalized
         *
         * @name _elements
         * @memberOf! Core.StackEqualizer
         * @type {functoin}
         * @private
          */
        _elements,

        /**
         * Normalize column heights
         *
         * @function _normalize
         * @memberOf! Core.StackEqualizer
         * @private
         */
        _normalize = function () {
            _context.each(function () {
                $(_elements, this).css("height", "auto");
            });
        },

        /**
         * Create equal height columns in a container
         *
         * @function equalize
         * @memberOf! Core.StackEqualizer
         * @param {object} context The element which columns need equalizing.
         * @param {function} selector Elements which need equalizing.
         * @public
         *
         * @fixme Returns erroneous results because images aren't always loaded in time.
         */
        equalize = function (context, selector) {
            var heights,
                items,
                tallest;

            context.each(function () {
                tallest = 0;

                // Get the items with the current context
                items = selector.apply($(this));

                // Create an array with all heights
                heights = items.map(function () {
                    return parseInt(this.scrollHeight, 10);
                });

                tallest = Math.max.apply(null, heights);

                // Set the actual height after calculating it
                items.css("height", tallest + "px");
            });
        },

        /**
         * Empty setup
         *
         * @function setup
         * @memberOf! Core.StackEqualizer
         * @public
         */
        setup = Core.noop;

    /**
     * Public API
     */
    return Core.register("StackEqualizer", {
        setup: setup,
        equalize: equalize
    });
}(Core || {}, jQuery));
