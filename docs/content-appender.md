

<!-- Start c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\modules\content-appender.js -->

## Core

@namespace ContentAppender

## _content

Data object representing the content to be appended

## _template

Template string to be compiled into HTML

## _context

The element that will have the new content appended to

## _contentAdded

'Only add our new content once'-flag

## addContent()

Add the content to the DOM by compiling the templayed template.

### Return:

* **Bool** Whether content was added or not.

## append(options)

Setup our DOM elements and content.

### Params: 

* **Object** *options* All options

## setup()

Empty setup

<!-- End c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\modules\content-appender.js -->

