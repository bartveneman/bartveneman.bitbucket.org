

<!-- Start c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\modules\content-arranger.js -->

## Core

@namespace ContentArranger

## _content

Contains initial flowing content

## _sidebar

Flowing content will flow into this

## _items

Flowing content elements

## _moveNode(node, into)

Remove a node from it's parent and append it to the 
designated container.

### Params: 

* **HTMLElement** *node* The node to move around.
* **HTMLElement** *into* The designated destination node.

### Return:

* **HTMLElement** The destination node into which the node

## expand()

Flow items into the designated container/sidebar/whatever.

### Return:

* **HTMLElement** The element where items have flown into

## collapse()

Flow items back into the initial container.

## setup()

Setup DOM elements.

<!-- End c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\modules\content-arranger.js -->

