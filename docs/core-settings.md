

<!-- Start c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\core-settings.js -->

## Core

@namespace CoreSettings

## ANIMATION_DURATION

The default animation duration

## BREAKPOINTS

Breakpoint reference. Should reflect those in CSS.

## mediaQuery(breakpoint, min)

Return a Media Query string

### Params: 

* **string** *breakpoint* A breakpoint from the list above
* **bool** *min* Whether to use min-width or max-width. Default: min-width.

### Return:

* **string** The complete media query string.

<!-- End c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\core-settings.js -->

