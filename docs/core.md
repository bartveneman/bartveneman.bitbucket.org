

<!-- Start c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\core.js -->

## Core

@namespace Core

## register(name, module)

Register a new module to the Core object

### Params: 

* **string** *name* Name of the module.
* **Object** *module* The actual module.

### Return:

* **Object** The updated Core.

## noop()

Empty function to use for callbacks etc. instead of having 
to make numerous empty functions yourself. Should save some 
memory.

<!-- End c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\core.js -->

