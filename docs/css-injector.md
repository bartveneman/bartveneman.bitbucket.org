

<!-- Start c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\modules\css-injector.js -->

## Core

@namespace CSSInjector

## _sheet

Reference to a document's dynamically inserted stylesheet
to which new rules can be applied.

## inject({string}, {string|array})

Inject method that puts new CSS in the document head.

### Params: 

* **selector** *{string}* The CSS selector
* **declarations** *{string|array}* A String for a single CSS declaration

## setup()

Setup this module

<!-- End c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\modules\css-injector.js -->

