

<!-- Start c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\modules\dom-helper.js -->

## Core

@namespace DOMHelper

## elementhasOverflow(element)

Check if an element's inner dimensions are larger than it's outer.

### Params: 

* **HTMLElement** *element* The element that should be checked.

### Return:

* **bool** True if inner dimensions are larger than outer

## setup()

Setup this module

<!-- End c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\modules\dom-helper.js -->

