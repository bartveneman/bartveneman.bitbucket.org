

<!-- Start c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\modules\mobilenav.js -->

## Core

@namespace Mobilenav

## _element

The element to which the scroll position is compared

## _navElement

The mobile navigation

## _toggler

The toggle button for the mobile navigation

## _window

The window element, cached

## _setMaxHeigth()

Calculate and set the maximum height of the Mobile Navigation.

### Return:

* **Number** The height in pixels.

## _applyStick()

Calculates position of the current window and the navigation
to apply a class to stick the element.

## _toggle()

Toggles the button and navigation state of the Mobile Navigation.

## _attachEvents()

Attach event handlers.

## setup()

Setup module: register DOM elements and initiate inner methods.
Only run on small devices that have the mobile nav enabled.

<!-- End c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\modules\mobilenav.js -->

