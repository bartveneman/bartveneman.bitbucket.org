

<!-- Start c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\modules\modal.js -->

## Core

@namespace Modal

## _backdrop

The semi transparent Modal backdrop/background

## _modal

The actual Modal element

## _modalStateClass

ClassName for the active modal

## _backdropStateClass

ClassName for the active backdrop

## _modalBodyClass

ClassName for the Modal body

## _maxHeight

The maximum height of the modal relative to the viewport
Corresponds to (_maxHeight * 10)vh in CSS

## _document

Cached jQuery-wrapped document

## _window

Cached jQuery-wrapped window

## _setBodyHeight()

Set the modal body height for browsers that don't support
the vh unit.

## _open()

Method to run after a '.js-togglemodal' was clicked.

## _close()

Hide the current modal and backdrop.

## _closeOnKey(event)

Close the modal with a keypress. Needs to be cached to event
handlers can be toggled on this function.

### Params: 

* **event** *event* Any keypress event

## _attachEvents()

Attach handler to modal togglers. Also adds a
resize handler if CSS vh unit is not supported.

## _createBackdrop()

Create a modal backdrop and add it to the page.

### Return:

* **HTMLElement** The backdrop element

## setup()

Setup this module

<!-- End c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\modules\modal.js -->

