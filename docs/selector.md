

<!-- Start c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\modules\selector.js -->

## Core

@namespace Selector

## _setPriceTableButtons()

Set disabled states to price table buttons if the row they
are in is not showing a scrollbar

## _attachEvents()

Attach event handlers

## setup()

Setup this module

<!-- End c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\modules\selector.js -->

