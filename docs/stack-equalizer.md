

<!-- Start c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\modules\stack-equalizer.js -->

## Core

@namespace StackEqualizer

## _context

The element in which the elements should be equalized

## _elements

Elements inside the context that should be equalized

## _normalize()

Normalize column heights

## equalize(context, selector)

Create equal height columns in a container

### Params: 

* **object** *context* The element which columns need equalizing.
* **function** *selector* Elements which need equalizing.

## setup()

Empty setup

Public API

<!-- End c:\Users\bart.veneman\Dropbox\Windesheim\M7 Afstuderen\reportal\assets\js\modules\stack-equalizer.js -->

